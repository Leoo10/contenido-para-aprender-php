<?php
// Aca va el skript de PHP

if ($_POST) {
    $valorA = $_POST['valorA'];
    $valorB = $_POST['valorB'];

    $suma = $valorA + $valorB;
    $multiplicacion = $valorA * $valorB;
    $division = $valorA / $valorB;


    echo "El resultado de la suma es = " . $suma . "<br>";
    echo "El resultado de la multiplicacion es = " . $multiplicacion . "<br>";
    echo "El resultado de la division es = " . $division . "<br>";
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculador con PHP</title>
</head>

<body>
    <form action="ejercicio5.php" method="post">
        Valor A:
        <input type="number" name="valorA"><br><br>
        Valor B:
        <input type="number" name="valorB"><br><br>
        <input type="submit" value="Calcular">
    </form>
</body>

</html>