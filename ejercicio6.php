<?php

class Persona
{
    public $nombre;
    private $edad;
    protected $altura;
    public function assignarNombre($nuevoNombre)
    {
        $this->nombre = $nuevoNombre;
    }
    public function mostrarNombre()
    {
        $this->edad = 20;
        return $this->edad;
    }
}

$leo = new Persona();

$leo->assignarNombre("Leonardo");


$myName = $leo->nombre;
$myAge = $leo->mostrarNombre();

echo $myName . " " . $myAge;
