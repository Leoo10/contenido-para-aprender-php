<?php

class Persona
{
    public $nombre;
    private $edad;
    protected $altura;
    public function assignarNombre($nuevoNombre)
    {
        $this->nombre = $nuevoNombre;
    }
    public function mostrarNombre()
    {
        $this->edad = 20;
        return $this->edad;
    }
}

class Trabajador extends Persona  // heredando de la clase de Persona
{
    public $puesto;
    public function presentarseComoTrabajador()
    {
        echo "Hola soy " . $this->nombre . " y soy un " . $this->puesto;
    }
}

$objPersona = new Persona(); // creando el objeto

$objPersona->assignarNombre("Leonardo"); // assignar al metodo de Persona

echo $objPersona->nombre; // consultando el la variable nombre

$objTrabajador = new Trabajador();

$objTrabajador->assignarNombre("Jasper");
$objTrabajador->puesto = "Enfermero";

echo $objTrabajador->nombre . "<br>";

$objTrabajador->presentarseComoTrabajador();
